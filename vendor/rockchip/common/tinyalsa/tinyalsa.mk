LOCAL_PATH := vendor/rockchip/common/tinyalsa

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/bin/amix:system/bin/amix

PRODUCT_COPY_FILES += \
  $(LOCAL_PATH)/lib/hw/audio.primary.rk30board.so:system/lib/hw/audio.primary.rk30board.so 

PRODUCT_COPY_FILES += \
  $(LOCAL_PATH)/lib64/hw/audio.primary.rk30board.so:system/lib64/hw/audio.primary.rk30board.so
